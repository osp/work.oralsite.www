import os.path
from fabric.api import run, local, put, cd, sudo, env, prefix
from fabric.contrib.console import confirm


env.hosts = ['sarma@185.26.126.221']
env.path = '/srv/sarma_data02/www/be.oralsite/app'


def deploy(branch='master'):
    """deploys to previously setup environment"""
    path_activate = '/srv/sarma_data02/www/be.oralsite/venv/bin/activate'
    path_wsgi = '/srv/sarma_data02/www/be.oralsite/app/oralsite/wsgi.py'

    with cd(env.path):
        run('git pull origin %s' % branch)

        with prefix('source %s' % path_activate):
            run('pip install -r requirements.txt')
            run('python manage.py collectstatic --noinput')

    run('touch %s' % path_wsgi)


def download():
    """synchronizes the local db from the remote one"""
    local('scp sarma@185.26.126.221:/srv/sarma_data02/www/be.oralsite/db/oralsite.db oralsite/')
